import React, { useCallback, useEffect, useState } from 'react';
import './App.css';
import useWebSocket, { ReadyState } from 'react-use-websocket';
import DataTable from './components/datatable';

const WS_URL = "ws:///117.4.244.187:13000/";
function App() {
    const [streaming, setStreaming] = useState(false);
    const [lastMessage, setLastMessage] = useState([]);
    const { sendJsonMessage, lastJsonMessage, readyState } =
      useWebSocket(WS_URL, {
        onOpen: () => console.log("WebSocket connection established."),
        shouldReconnect: (closeEvent) => true,
      });


    useEffect(() => {
      if (lastJsonMessage !== null) {
        console.log(lastJsonMessage);
        setLastMessage((prev) => prev.concat(lastMessage));
        startStreaming();
      }
    }, [lastJsonMessage]);

    const startStreaming = useCallback(() => {
      setStreaming(true);
      const sendFrame = () => {
        const message = {
          eventTime: new Date().toISOString(),
          eventType: "Motion Detection",
          cameraLocation: {
            name: "Main Entrance",
            coordinates: {
              latitude: 40.7128,
              longitude: -74.006,
            },
          },
          imageVideo: "event_clip.mp4",
          priorityLevel: "High",
          detectionDetails: "Image analysis for motion",
          eventDescription: "Unidentified person at the main entrance",
          cameraID: Math.floor(Math.random() * 100),
          networkInfo: {
            relatedCameras: [Math.floor(Math.random() * 100), Math.floor(Math.random() * 100)],
            networkStatus: "Online",
          },
          adminUserInfo: {
            userID: "admin01",
            notificationStatus: "Sent",
          },
          resolutionQuality: {
            resolution: "1080p",
            quality: "High",
          },
          additionalNotes: "Person wearing a red jacket",
        };

        sendJsonMessage({
          event: "live-stream",
          data: message,
        });
      };

      setInterval(sendFrame, 100);
    }, [lastJsonMessage]);


    const connectionStatus = {
      [ReadyState.CONNECTING]: "Connecting",
      [ReadyState.OPEN]: "Open",
      [ReadyState.CLOSING]: "Closing",
      [ReadyState.CLOSED]: "Closed",
      [ReadyState.UNINSTANTIATED]: "Uninstantiated",
    }[readyState];
  return (
    <div className="App">
      <div className="event-table">
        <DataTable/>
      </div>
      <div>
        <span>The WebSocket is currently {connectionStatus}</span>
        <div className="wrap">
          <div>
            <h1>Broadcast</h1>
            {lastMessage && <p>Last Message: {JSON.stringify(lastMessage)}</p>}
          </div>
        </div>
        <div>
          {streaming ? null : (
            <button onClick={startStreaming}>Start Streaming</button>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
